#include <sysexits.h>
#include <iostream>
#include <string>

#include <stdlib.h>

double** readMatrix(int, int);
double** multMatrix(double** matrix1, int matrix1_rows, int matrix1_cols,
                    double** matrix2, int matrix2_rows, int matrix2_cols);

int main(int argc, char const *argv[]) {
  if (argc - 1 != 4) {
    std::cerr << "Error: Usage: " << argv[0] << " matrix1_rows matrix1_cols matrix2_rows matrix2_cols" << std::endl;
    exit(EX_USAGE);
  }

  int matrix1_rows = atoi(argv[1]);
  int matrix1_cols = atoi(argv[2]);
  int matrix2_rows = atoi(argv[3]);
  int matrix2_cols = atoi(argv[4]);

  if (matrix1_cols != matrix2_rows) {
    std::cerr << "Error: matrix1_cols and matrix2_rows must be equal" << std::endl;
    exit(EX_DATAERR);
  }

  double** matrix1 = readMatrix(matrix1_rows, matrix1_cols);
  double** matrix2 = readMatrix(matrix2_rows, matrix2_cols);

  double** matrix_mult = multMatrix(matrix1, matrix1_rows, matrix1_cols,
                                    matrix2, matrix2_rows, matrix2_cols);

  for (int i = 0; i < matrix1_rows; i++) {
    for (int j = 0; j < matrix2_cols; j++) {
      std::cout << matrix_mult[i][j] << ' ';
    }
    std::cout << '\n';
  }

  return 0;
}

double** readMatrix(int rows, int cols) {
  double** matrix = new double*[rows];
  for (int i = 0; i < rows; i++) {
    matrix[i] = new double[cols];
    for (int j = 0; j < cols; j++) {
      std::cin >> matrix[i][j];
    }
  }
  return matrix;
}

double** multMatrix(double** matrix1, int matrix1_rows, int matrix1_cols,
                    double** matrix2, int matrix2_rows, int matrix2_cols) {
  int new_rows = matrix1_rows, new_cols = matrix2_cols;
  double** new_matrix = new double*[new_rows];
  for (int i = 0; i < new_rows; i++) {
    new_matrix[i] = new double[new_cols];
    for (int j = 0; j < new_cols; j++) {

      double sum = 0;
      for (int k = 0; k < matrix1_cols; k++) {
        sum += matrix1[i][k] * matrix2[k][j];
      }

      new_matrix[i][j] = sum;
    }
  }
  return new_matrix;
}
