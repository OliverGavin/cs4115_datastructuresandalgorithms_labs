
#include <iostream>
#include <string>
#include <sstream>
#include <list>
#include <math.h>
#include <sysexits.h>
#include <stdlib.h>

using std::cin;
using std::cout;
using std::cerr;
using std::endl;
using std::string;
using std::list;

class nz {	    // one instance for every non-zero entry in matrix
public:
  nz(int col, double val) {
    this->col = col;
    this->val = val;
  }
  int col;
  double val;
};

typedef list<nz> sparseRow;	// a single row
typedef list<sparseRow> sparseMatrix;	// a matrix

sparseMatrix getSparseMatrix();
sparseMatrix transposeSparseMatrix(const sparseMatrix & matrix);
double dotProductSparseRow(const sparseRow & rowA, const sparseRow & rowB);
sparseMatrix multiplySparseMatrix(const sparseMatrix & matrix, const sparseMatrix & trans);
sparseMatrix powerSparseMatrix(const sparseMatrix & matrix, int power);
void printSparseMatrix(const sparseMatrix & matrix);

//
//  Oliver Gavin
//  Assumes input is read separately to transposing
//  Lists of lists are used assuming we don't know the size of the matrix or that it is a square (a correction is made for the purpose of handin tests assuming it is a square)
//
int main(int argc, char *argv[])
{
  if (argc - 1 != 1) {
    std::cerr << "Error: Usage: " << argv[0] << " power" << std::endl;
    exit(EX_USAGE);
  }

  // if (!std::isdigit(argv[1])) {
  //   std::cerr << "Error: power must be an integer" << std::endl;
  //   exit(EX_DATAERR);
  // }

  int power = atoi(argv[1]);

  if (power < 0) {
    std::cerr << "Error: power must be an positive integer" << std::endl;
    exit(EX_DATAERR);
  }

  sparseMatrix matrix = getSparseMatrix();  // Note: While it would be possible to read the matrix and transpose it all in one
                                            //       I think it is better practice to handle input separate to processing

  sparseMatrix powered = powerSparseMatrix(matrix, power);

  printSparseMatrix(powered);
}

sparseMatrix getSparseMatrix() {
  string line;
  sparseMatrix matrix;  // list of lists

  while (getline(cin, line))	// get next full line of text
  {
    std::istringstream lstream(line);
    sparseRow row;  // a single row in the matrix
    double val;
    int col;

    while (lstream >> col >> val)	// peel off values in this line, one at a time adding to the row
    {
      nz el = nz(col, val);
      row.push_back(el);
    }

    matrix.push_back(row);  // Add the row to the matrix

  }

  return matrix;
}

sparseMatrix transposeSparseMatrix(const sparseMatrix & matrix) {
  sparseMatrix trans;
  if (matrix.size() == 0)
    return trans;

  sparseRow t;
  trans.push_back(t);
  sparseMatrix::const_iterator m_iter;  // Iterator for each row in the matrix
  int mr_index = 0;                     // Store the current row number
  for (m_iter = matrix.begin(); m_iter != matrix.end(); m_iter++)
  {
    // Store the row of the matrix
    sparseRow mrow = *m_iter;

    // Set up an iterator for the transposed matrix
    sparseMatrix::iterator tr_iter = trans.begin();
    int tr_index = 0;

    // iterate over matrix row's columns
    sparseRow::const_iterator mr_iter;
    for (mr_iter = mrow.begin(); mr_iter != mrow.end(); mr_iter++)
    {
      int col = mr_iter->col;
      double val = mr_iter->val;

      // Creates the number of required rows in the transposed matrix if they do not exist
      while (trans.size() < col) {
        sparseRow trow;
        trans.push_back(trow);
      }
      // Advance the transposed matrix iterator until the row is equal to the column in the input matrix row
      // Note: since the elements in the row of the original matrix are in order
      //       we can use the same iterator across iterations, just advancing it as required
      while (tr_index < col-1) {  // Off by one fix since input index starts at 1
        tr_index++;
        tr_iter++;
      }

      // Add the element from the original matrix to the correct row of the transposed matrix
      sparseRow& trow = *tr_iter;
      nz el = nz(mr_index+1, val);  // Off by one fix since input index starts at 1
      trow.push_back(el);
    }
    mr_index++;
  }

  // Since this is a square matrix, make sure to fill it up even without more data
  // Note: this could be ommited if we didn't assume a square matrix (the code above assumes we do not know the size of the matrix)
  //       Added for the purpose of handin tests
  while (trans.size() < matrix.size()) {
    sparseRow trow;
    trans.push_back(trow);
  }
  return trans;
}

double dotProductSparseRow(const sparseRow & rowA, const sparseRow & rowB) {
  double dotProduct = 0;

  sparseRow::const_iterator a_iter = rowA.begin();
  sparseRow::const_iterator b_iter = rowB.begin();

  while (a_iter != rowA.end() && b_iter != rowB.end()) {
    if (a_iter->col == b_iter->col) {
      dotProduct += a_iter->val * b_iter->val;
      a_iter++;
      b_iter++;
    } else if (a_iter->col < b_iter->col) {
      a_iter++;
    } else {
      b_iter++;
    }
  }

  return dotProduct;
}

sparseMatrix multiplySparseMatrix(const sparseMatrix & matrix, const sparseMatrix & trans) {
  sparseMatrix mult;

  sparseMatrix::const_iterator m_iter = matrix.begin();
  while (m_iter != matrix.end()) {

    sparseRow row;
    int t_row = 1;

    sparseMatrix::const_iterator t_iter = trans.begin();
    while (t_iter != trans.end()) {

      // row = row of m
      // col = row of t

      double val = dotProductSparseRow(*m_iter, *t_iter);
      if (val != 0) {
        int col = t_row;
        nz el = nz(col, val);
        row.push_back(el);
      }

      t_iter++;
      t_row++;
    }

    mult.push_back(row);
    m_iter++;
  }

  return mult;
}

sparseMatrix powerSparseMatrix(const sparseMatrix & matrix, int power) {

  if (power == 0) {
  	// deal with the case where power is 0. The returns an identity matrix.
    sparseMatrix temp;
    for (int i = 1; i <= matrix.size(); i++) {
      sparseRow row;
      nz el = nz(i, 1);
      row.push_back(el);
      temp.push_back(row);
    }
    return temp;
  }

  sparseMatrix temp = transposeSparseMatrix(matrix);	// Transpose of initial matrix
  sparseMatrix powered = matrix;											// Base case/initial value

  power--;  // Since we initialise with original matrix

  while (power > 0) {
    if (power%2 == 1)
      powered = multiplySparseMatrix(powered, temp);

    sparseMatrix trans = transposeSparseMatrix(temp);
    temp = multiplySparseMatrix(temp, trans);		// Multiply temp by its transpose i.e. square it
    power /= 2;

  }

  return powered;
}

void printSparseMatrix(const sparseMatrix & matrix) {
  // now iterate through the list of lists, writing out as we go
  sparseMatrix::const_iterator m_iter;
  for (m_iter = matrix.begin(); m_iter != matrix.end(); m_iter++)
  {
    // input
    sparseRow row = *m_iter;

    // output
    std::ostringstream compressed_line;

    // iterate over this row, writing out
    sparseRow::const_iterator r_iter;
    for (r_iter = row.begin(); r_iter != row.end(); r_iter++)
    {
      compressed_line << r_iter->col << ' ' << r_iter->val << ' ';
    }

    std::cout << compressed_line.str() << std::endl;

  }
}
