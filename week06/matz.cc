#include <sysexits.h>
#include <iostream>
#include <sstream>
#include <string>

#include <stdlib.h>

double** readMatrix(int, int);

int main(int argc, char const *argv[]) {
  if (argc - 1 != 2) {
    std::cerr << "Error: Usage: " << argv[0] << " matrix_rows matrix_cols" << std::endl;
    exit(EX_USAGE);
  }

  int matrix_rows = atoi(argv[1]);
  int matrix_cols = atoi(argv[2]);

  if (matrix_cols != matrix_rows) {
    std::cerr << "Error: matrix_cols and matrix_rows must be equal" << std::endl;
    exit(EX_DATAERR);
  }

  double** matrix = readMatrix(matrix_rows, matrix_cols);

  std::cout << matrix_rows << '\n';

  int nz_total = 0;
  for (int i = 0; i < matrix_rows; i++) {
    int nz = 0;
    std::ostringstream node_string;
    for (int j = 0; j < matrix_cols; j++) {
      if (matrix[i][j] != 0) {
        node_string << " " << (j+1) << " " << matrix[i][j];
        nz++;
      }
    }

    std::cout << nz << node_string.str() << " \n";

    nz_total += nz;
  }

  std::cout << nz_total << std::endl;

  return 0;
}

double** readMatrix(int rows, int cols) {
  double** matrix = new double*[rows];
  for (int i = 0; i < rows; i++) {
    matrix[i] = new double[cols];
    for (int j = 0; j < cols; j++) {
      std::cin >> matrix[i][j];
    }
  }
  return matrix;
}
