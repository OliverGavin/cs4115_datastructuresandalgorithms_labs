#include <sysexits.h>
#include <iostream>
#include <sstream>
#include <string>
#include <cmath>

int main(int argc, char const *argv[]) {

  double epsilon = 0;
  if (argc > 1 && std::string(argv[1]) == "-e")
    epsilon = fabs(strtod(argv[2], 0));

  std::string line;
  while (getline(std::cin, line)) {

    // input
    std::istringstream lstream(line);
    double val;

    // output
    std::ostringstream compressed_line;

    int col = 1;
    while (lstream >> val) {
      if (val > epsilon || val < -epsilon)
        compressed_line << col << ' ' << val << ' ';
      col++;
    }

    std::cout << compressed_line.str() << std::endl;

  }

  return 0;
}
