
#include <iostream>
#include <string>
#include <sstream>
#include <list>

using std::cin;
using std::cout;
using std::cerr;
using std::endl;
using std::string;
using std::list;

#include <math.h>
#include <stdlib.h>

class nz {	    // one instance for every non-zero entry in matrix
public:
  nz(int col, double val) {
    this->col = col;
    this->val = val;
  }
  int col;
  double val;
};

typedef list<nz> sparseRow;	// a single row
typedef list<sparseRow> sparseMatrix;	// a matrix

double epsilon = 0.0;

int main(int argc, char *argv[])
{
  if (argc > 1 && string(argv[1]) == "-e")
    epsilon = fabs(strtod(argv[2], 0));

  string line;

  sparseMatrix matrix;

  while (getline(cin, line))	// get next full line of text; NB: text
  {
    std::istringstream lstream(line);
    sparseRow row;
    double val;

    int col = 0;
    while (lstream >> val)	// peel off values in this line, one at a time
    {
      col++;
      if (fabs(val) <= epsilon)
	     continue;

      // ok this is a keeper; so keep it
      nz el = nz(col, val);
      // std::cout << el.val << ' ' << '\n';
      row.push_back(el);
    }

    matrix.push_back(row);

  }

  // now iterate through the list of lists, writing out as we go
  sparseMatrix::const_iterator m_iter; // we only want to /read/ list
  for (m_iter = matrix.begin(); m_iter != matrix.end(); m_iter++)
  {
    // input
    sparseRow row = *m_iter;

    // output
    std::ostringstream compressed_line;

    // iterate over this row, writing out
    sparseRow::const_iterator r_iter;
    for (r_iter = row.begin(); r_iter != row.end(); r_iter++)
    {
      compressed_line << r_iter->col << ' ' << r_iter->val << ' ';
    }

    std::cout << compressed_line.str() << std::endl;

  }
}

// STL:  http://www.csci.csusb.edu/dick/samples/stl.html
